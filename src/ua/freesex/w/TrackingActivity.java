/*Такс такс такс, шо у нас тут? 
 Опен соурс, наканецта.
 %ЛИЦЕНЗИЯ%
 Код принадлежит Максиму Андрющенко!
 Итак, братишка, чтобы не было непоняток сразу скажу:
 1. Не выдавай проект за свой, узнаю, найду, оболью ебло кислотой!!
 2. Всё написано в Eclipse, не пытайтест просить перевести на Gradle!
 3. Удачи!*/
package ua.freesex.w;

import android.app.*;
import android.os.*;
import com.google.analytics.tracking.android.*;
import android.view.*;

public class TrackingActivity extends Activity
{
	private EasyTracker tracker;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(
			WindowManager.LayoutParams.FLAG_FULLSCREEN,
			WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	@Override
	public void onStart()
	{
		tracker.getInstance(this).activityStart(this);
		super.onStart();
	}
	@Override
	public void onStop()
	{
		tracker.getInstance(this).activityStop(this);
		super.onStop();
	}
}
