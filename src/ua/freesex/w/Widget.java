package ua.freesex.w;

import android.appwidget.*;
import android.content.*;
import android.widget.*;
import java.util.*;
import android.util.*;

public class Widget extends AppWidgetProvider
{
	private Context context;
	private AppWidgetManager a;
	private int updates = 0;
	private int[] appwidgetIds = null;
	private String Update = null;


	public void onReceive(Context ctn, Intent inte, String value)
	{
		super.onReceive(ctn, inte);
		Log.d(value, "receive");
	}
	public void onUpdate(Context context, AppWidgetManager a)
	{
		++updates;
		super.onUpdate(context, a, appwidgetIds);
		Log.d(Update, "Widget update" + updates + "");
		RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget);
		Random rnd = new Random();
		int frases [] = {
			1, 2, 3,
			4, 5, 6
		};
		int pos = rnd.nextInt(frases.length);

		if (pos == 1)
		{
			views.setTextViewText(R.id.tv_q, "" + R.string.wp_1);
		}
		if (pos == 2)
		{
			views.setTextViewText(R.id.tv_q, "" + R.string.wp_2);
		}
		if (pos == 3)
		{
			views.setTextViewText(R.id.tv_q, "" + R.string.wp_3);
		}
		if (pos == 4)
		{
			views.setTextViewText(R.id.tv_q, "" + R.string.wp_4);
		}
		if (pos == 5)
		{
			views.setTextViewText(R.id.tv_q, "" + R.string.wp_5);
		}
		if (pos == 6)
		{
			views.setTextViewText(R.id.tv_q, "" + R.string.wp_6);
		}
	}
	public void onEnabled(Context c, String enable)
	{
		super.onEnabled(c);
		Log.d(enable, "enable");
		
	}
}
